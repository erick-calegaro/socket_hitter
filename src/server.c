#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <ctype.h>
#include <time.h>
#include <pthread.h>
#include "sqlite3.h"
#include <curl/curl.h>

//"Makefile" XGH
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"
#pragma GCC diagnostic ignored "-Wpointer-to-int-cast"
#include "header.h"
#include "def_types.h"
#include "utl_sqlite.h"
#include "utl_sqlite.c"
#include "utils.c"
#include "isoProcess.c"

void *connection_handler(void *);
int   postQrCode(char * qrCode, int size);
int   getResult();
int   setResult();

int main(int argc, char* argv[]) {

    //Variaveis auxiliares para encontrar o arquivo a ser transferido.
    struct dirent *myfile;
    struct stat mystat;
    //verificando se foi executando o comando corretamente
    if (argc != 2) {
        fprintf(stderr, "use:./server [Porta]\n");
        return -1;
    } else if (!isdigit(*argv[1])) {
        fprintf(stderr, "Argumento invalido '%s'\n", argv[1]);
        fprintf(stderr, "use:./server [Porta]\n");
        return -1;
    }

    char* aux1 = argv[1];
    int portaServidor = atoi(aux1);

   //variaveis
    int socket_desc, conexao, c, nova_conex;
    struct sockaddr_in servidor, cliente;
    char *mensagem;
    char tempRecvMessage[MAX_MSG] = {0x00};
    char recvMessage[MAX_MSG] = {0x00};
    char sendMessage[MAX_MSG] = {0x00};
    int tamanho, count, sizeRecv, sizeSend, isfirstMsg, sizeTotal;

    // para pegar o IP e porta do cliente  
    char *cliente_ip;
    int cliente_port;

    //*********************************************************************//
    //      INICIO DO TRATAMENTO DA THREAD, localização e transferência    // 
    //      do arquivo.                                                    // 
    //*********************************************************************//
#ifndef LOCAL
    void *connection_handler(void *socket_desc) {
        /*********************************************************/
        /*********comunicao entre cliente/servidor****************/

        // pegando IP e porta do cliente
        cliente_ip = inet_ntoa(cliente.sin_addr);
        cliente_port = ntohs(cliente.sin_port);
        printf("cliente conectou: %s : [ %d ]\n", cliente_ip, cliente_port);
        isfirstMsg = TRUE;

        // lendo dados enviados pelo cliente
        //mensagem 1 recebido nome do arquivo   
        memset(tempRecvMessage, 0x00, sizeof(tempRecvMessage));
        if ((sizeRecv = read(conexao, tempRecvMessage, MAX_MSG)) < 0) {
            perror("Erro ao receber dados do cliente: ");
            return NULL;
        }

        if (tempRecvMessage == NULL){
            perror("Erro ao receber dados do cliente");
            return NULL;   
        }

        printf("message result = [%s]\n", tempRecvMessage);
        util_String_DumpStringLog(tempRecvMessage, 100);


        do{
            if (strlen(tempRecvMessage) == 4 || strlen(tempRecvMessage) == 5){  //ping
                write(conexao, "pong", 4);
                break;
            }

            if (strlen(tempRecvMessage) == 6 || strlen(tempRecvMessage) == 7){   //paynet
                for (int timeout = 0; timeout < 10; timeout++)
                    usleep(1000*1000);
                write(conexao, "captura", 7);
                break;
            }

            if (strlen(tempRecvMessage) < 16){      //Workaround pra não quebrar logica do QRCode
                char serialNumber[10];
                strcpy(serialNumber, tempRecvMessage);
                int result = 0;
                mensagem = "0";
                setResult(serialNumber);
                // wait
                for (int timeout = 0; timeout < 30; timeout++){
                    result = getResult(serialNumber);
                    if      (result == 1){ mensagem = "1"; break;}
                    else if (result == 2){ mensagem = "2"; break;}
                    usleep(1000*1000);
                }
                write(conexao, mensagem, strlen(mensagem));
            }else{                              // processamento de ISO
                printf("Size recv [%d]\n", sizeRecv);
                int result = 0;
                do{
                    if (isfirstMsg){
                        isfirstMsg = FALSE;
                        tamanho = util_ConvertToDecimal_Header(tempRecvMessage) - 4; //tirando cabeçalho
                        strcpy(recvMessage, tempRecvMessage);
                    }else{
                        if (sizeRecv < tamanho){
                            memset(tempRecvMessage, 0x00, sizeof(tempRecvMessage));
                            sizeRecv += read(conexao, tempRecvMessage, MAX_MSG);
                            printf("Passou read[%d] = [%s]\n", sizeRecv, tempRecvMessage);
                            strcat(recvMessage, tempRecvMessage);
                        }else{
                            break;
                        }
                    }
                }while(TRUE);
                //!todo: tratar erros de formato ao receber iso
                result = iso_MainProcess(&recvMessage[4], tamanho, sendMessage, &sizeSend);
                printf("sendMessage(%d) = [%s]\n", sizeSend, sendMessage);

                if (!result)
                    write(conexao, sendMessage, sizeSend);

                //Pegar a mensagem, adicionar tamanho ao cabeçalho e enviar de volta
            }
        }while(FALSE);
        
        usleep(1000*1000);
        close(conexao);
        printf("Servidor finalizado...\n");
        return NULL;
    }
#endif

    //*********************************************************************//
    //      FIM DO TRATAMENTO DA THREAD, localização e transferencia    // 
    //      do arquivo.                                                    // 
    //*********************************************************************//

    do{
        //************************************************************
        /*********************************************************/
        //Criando um socket
        socket_desc = socket(AF_INET, SOCK_STREAM, 0);
        if (socket_desc == -1) {
            printf("Nao foi possivel criar o socket, aguarde alguns instantes\n");
            return -1;
        }

        //Preparando a struct do socket
        servidor.sin_family = AF_INET;
        servidor.sin_addr.s_addr = INADDR_ANY; // Obtem IP do S.O.
        servidor.sin_port = htons(portaServidor);

        //Associando o socket a porta e endereco
        if (bind(socket_desc, (struct sockaddr *) &servidor, sizeof (servidor)) < 0) {
            puts("Porta ocupada, aguarde alguns instantes\n");
            return -1;
        }
        puts("Bind efetuado com sucesso\n");

        // Ouvindo por conexoes
        listen(socket_desc, 3);
        /*********************************************************/

        //Aceitando e tratando conexoes

        puts("Aguardando por conexoes...");
        c = sizeof (struct sockaddr_in);

        while ((conexao = accept(socket_desc, (struct sockaddr *) &cliente, (socklen_t*) & c))) {
            if (conexao < 0) {
                perror("Erro ao receber conexao\n");
                continue;
            }
#ifndef LOCAL
            pthread_t sniffer_thread;
            nova_conex = (int) malloc(1);
            nova_conex = conexao;

            if (pthread_create(&sniffer_thread, NULL, connection_handler, (void*) nova_conex) < 0) {
                perror("could not create thread");
                continue;
            }
#else
        // pegando IP e porta do cliente
        cliente_ip = inet_ntoa(cliente.sin_addr);
        cliente_port = ntohs(cliente.sin_port);
        printf("cliente conectou: %s : [ %d ]\n", cliente_ip, cliente_port);
        isfirstMsg = TRUE;

        // lendo dados enviados pelo cliente
        //mensagem 1 recebido nome do arquivo   
        memset(tempRecvMessage, 0x00, sizeof(tempRecvMessage));
        if ((sizeRecv = read(conexao, tempRecvMessage, MAX_MSG)) < 0) {
            perror("Erro ao receber dados do cliente: ");
            continue;
        }

        if (tempRecvMessage == NULL){
            perror("Erro ao receber dados do cliente");
            continue;   
        }

        printf("message result = [%s]\n", tempRecvMessage);
        util_String_DumpStringLog(tempRecvMessage, 100);


        do{
            if (strlen(tempRecvMessage) == 4 || strlen(tempRecvMessage) == 5){      //ping
                write(conexao, "pong", 4);
                break;
            }

            if (strlen(tempRecvMessage) == 6 || strlen(tempRecvMessage) == 7){   //paynet
                for (int timeout = 0; timeout < 10; timeout++)
                    usleep(1000*1000);
                write(conexao, "captura", 7);
                break;
            }

            if (strlen(tempRecvMessage) < 16){      //Workaround pra não quebrar logica do QRCode
                char serialNumber[10];
                strcpy(serialNumber, tempRecvMessage);
                int result = 0;
                mensagem = "0";
                setResult(serialNumber);
                // wait
                for (int timeout = 0; timeout < 30; timeout++){
                    result = getResult(serialNumber);
                    if      (result == 1){ mensagem = "1"; break;}
                    else if (result == 2){ mensagem = "2"; break;}
                    usleep(1000*1000);
                }
                write(conexao, mensagem, strlen(mensagem));
            }else{                              // processamento de ISO
                printf("Size recv [%d]\n", sizeRecv);
                int result = 0;
                do{
                    if (isfirstMsg){
                        isfirstMsg = FALSE;
                        tamanho = util_ConvertToDecimal_Header(tempRecvMessage) - 4; //tirando cabeçalho
                        strcpy(recvMessage, tempRecvMessage);
                    }else{
                        if (sizeRecv < tamanho){
                            memset(tempRecvMessage, 0x00, sizeof(tempRecvMessage));
                            sizeRecv += read(conexao, tempRecvMessage, MAX_MSG);
                            printf("Passou read[%d] = [%s]\n", sizeRecv, tempRecvMessage);
                            strcat(recvMessage, tempRecvMessage);
                        }else{
                            break;
                        }
                    }
                }while(TRUE);
                //!todo: tratar erros de formato ao receber iso
                result = iso_MainProcess(&recvMessage[4], tamanho, sendMessage, &sizeSend);
                printf("sendMessage(%d) = [%s]\n", sizeSend, sendMessage);

                if (!result)
                    write(conexao, sendMessage, sizeSend);

                //Pegar a mensagem, adicionar tamanho ao cabeçalho e enviar de volta
            }
        }while(FALSE);
        
        usleep(1000*1000);
        close(conexao);
        printf("Servidor finalizado...\n");
        continue;
#endif
            puts("Handler assigned");
        }
        if (nova_conex < 0) {
            perror("accept failed");
            continue;
        }
    }while(1);
    
    return 0;
}

struct MemoryStruct {
  char *memory;
  size_t size;
};
 
static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;
 
  char *ptr = realloc(mem->memory, mem->size + realsize + 1);
  if(!ptr) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }
 
  mem->memory = ptr;
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;
 
  return realsize;
}

int postQrCode(char * qrCode, int size)
{
    CURL *curl_handle;
    CURLcode res;

    char zAux[80] = {0x00};
    char sAux[32] = {0x00};

    memcpy(sAux, qrCode, size);

    struct MemoryStruct chunk;
    chunk.memory = malloc(1);  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */

    curl_global_init(CURL_GLOBAL_ALL);
    curl_handle = curl_easy_init();
    
    curl_easy_setopt(curl_handle, CURLOPT_URL, "http://tinywebdb.appinventor.mit.edu/storeavalue");
    sprintf(zAux, "tag=mp30_qrcode&value=%s&fmt=html", sAux);
    curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, zAux);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
    res = curl_easy_perform(curl_handle);

    if(res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
        curl_easy_strerror(res));
    }
    else {
        printf("%lu bytes retrieved\n", (unsigned long)chunk.size);
    }

    curl_easy_cleanup(curl_handle);
    free(chunk.memory);
    curl_global_cleanup();

    return 0;
}

int setResult(char * term)
{
    CURL *curl_handle;
    CURLcode res;

    char zAux[80] = {0x00};
    char sAux[32] = {0x00};

    memcpy(sAux, term, 8);
    sprintf(zAux, "tag=mp30_%s&value=0&fmt=html", sAux);
    printf("%s\n", zAux);

    struct MemoryStruct chunk;
    chunk.memory = malloc(1);  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */

    curl_global_init(CURL_GLOBAL_ALL);
    curl_handle = curl_easy_init();
    
    curl_easy_setopt(curl_handle, CURLOPT_URL, "http://tinywebdb.appinventor.mit.edu/storeavalue");
    curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, zAux);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
    res = curl_easy_perform(curl_handle);

    if(res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
        curl_easy_strerror(res));
    }

    curl_easy_cleanup(curl_handle);
    free(chunk.memory);
    curl_global_cleanup();

    return 0;
}

int getResult(char * term)
{
    CURL *curl_handle;
    CURLcode res;
    int result = 0;

    char zAux[80] = {0x00};
    char sAux[32] = {0x00};

    memcpy(sAux, term, 8);
    sprintf(zAux, "tag=mp30_%s&fmt=html", sAux);

    struct MemoryStruct chunk;
    chunk.memory = malloc(1);  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */

    curl_global_init(CURL_GLOBAL_ALL);
    curl_handle = curl_easy_init();
    
    curl_easy_setopt(curl_handle, CURLOPT_URL, "http://tinywebdb.appinventor.mit.edu/getvalue");
    curl_easy_setopt(curl_handle, CURLOPT_POSTFIELDS, zAux);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
    res = curl_easy_perform(curl_handle);

    if(res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
        curl_easy_strerror(res));
        result = 0;
    }
    else {
        printf("%s\n", zAux);
        printf("%lu bytes retrieved\n", (unsigned long)chunk.size);
        printf("[%c]\n", chunk.memory[102]);
        result = chunk.memory[102] - 48;
        if (result < 0 || result > 2){
            result = 0;
        }
    }

    curl_easy_cleanup(curl_handle);
    free(chunk.memory);
    curl_global_cleanup();

    return result;
}