# README #

* Este é um server TCP/IP simples utilizado para rebater mensagens ISO-8583 de pagamentos.

### Para que serve este repositorio? ###

* Utilizado em conjunto com a aplicação DEMO-POS permite transações simuladas em ambiente online.

### Como faço para configurar? ###

#### Requisitos:
* Estar executando um SO Linux
* Ter instalado o gcc

#### Configuração:
1. Executar o clone do repositorio
2. Executar o arquivo "compiler.sh" para obter o executavel para AWS
* Caso for utilizar localmente utilize o shell "localCompiler.sh"

### Como faço para executar? ###

* Rodar o comando "./server <port>"
* Ou executar o shell "runner.sh" para ouvir a porta 7071

### Com quem posso falar? ###
[Linkedin](https://www.linkedin.com/in/erick-calegaro/)